using RiptideNetworking;
using RiptideNetworking.Utils;
using System;
using UnityEngine;


/*
 * Messages enum.
 * Riptide give ids to messages to distinguish between types of messages (classic).
 * The [MessageHandler((ushort)ServerToClientId.<id>] allow us to associate a method with a message to handle this message upon reception.
 */
public enum ServerToClientId : ushort
{
    playerSpawned = 1,
    playerMovement = 2,
}

public enum ClientToServerId : ushort
{
    name = 1,
    input = 2,
}

public class NetworkManager : MonoBehaviour
{
    private static NetworkManager _singleton;
    public static NetworkManager Singleton
    {
        get => _singleton;
        private set
        {
            if (_singleton == null)
                _singleton = value;
            else if (_singleton != value)
            {
                Debug.Log($"{nameof(NetworkManager)} instance already exists, destroying duplicate!");
                Destroy(value);
            }
        }
    }

    // Riptide class that represents a client and holds all methods related to it.
    public Client Client { get; private set; }

    private string username;

    private void Awake()
    {
        Singleton = this;
    }

    private void Start()
    {
        // connect riptide logging capabilities to unity console
        RiptideLogger.Initialize(Debug.Log, Debug.Log, Debug.LogWarning, Debug.LogError, false);

        // Riptide have client methods, and we can subscribe our custom methods to the client methods so that they are called.
        Client = new Client();
        Client.Connected += DidConnect;
        Client.ConnectionFailed += FailedToConnect;
        Client.ClientDisconnected += PlayerLeft;
        Client.Disconnected += DidDisconnect;
    }

    private void FixedUpdate()
    {
        Client.Tick();
    }

    private void OnApplicationQuit()
    {
        Client.Disconnect();
    }

    public void Connect(string username, string ip)
    {
        ip = (ip == "" ? "127.0.0.1:7777" : ip);
        Client.Connect($"{ip}");
        this.username = username;
    }

    private void DidConnect(object sender, EventArgs e)
    {
        Message message = Message.Create(MessageSendMode.reliable, ClientToServerId.name);
        message.AddString(username);
        Client.Send(message);
    }

    private void FailedToConnect(object sender, EventArgs e)
    {
        UIManager.Singleton.BackToMain();
    }

    private void PlayerLeft(object sender, ClientDisconnectedEventArgs e)
    {
        if (Player.list.TryGetValue(e.Id, out Player player))
            Destroy(player.gameObject);
    }

    private void DidDisconnect(object sender, EventArgs e)
    {
        UIManager.Singleton.BackToMain();
        foreach (Player player in Player.list.Values)
            Destroy(player.gameObject);
    }
}
