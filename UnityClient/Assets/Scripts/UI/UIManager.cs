using RiptideNetworking;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    // singleton -> une instance attach�e � un gameobject, utilisable partout dans le reste du projet?
    private static UIManager _singleton;
    public static UIManager Singleton
    {
        // sera utilis�e par toutes les parties du code qui veulent acc�der � cette instance
        get => _singleton;
        // called by unity upon start, if multiple gameobjects have this script attached, an attemps to instantiate multiple times this 
        // singleton will happen, which we don't allow.
        private set
        {
            if (_singleton == null)
                _singleton = value;
            else if (_singleton != value)
            {
                Debug.Log($"{nameof(UIManager)} instance already exists, destroying duplicate!");
                Destroy(value);
            }
        }
    }

    // Allow to give structure to the inspector tab.
    [Header("Connect")]
    // Display private field in inspector without exposing them to other scripts.
    [SerializeField] private GameObject connectUI;
    [SerializeField] private InputField usernameField;
    [SerializeField] private InputField ipField;

    private void Awake()
    {
        Singleton = this;
    }
    
    // When the connect button is clicked, the UI disappeared and we ask the network manager to make the connection.
    public void ConnectClicked()
    {
        usernameField.interactable = false;
        connectUI.SetActive(false);

        NetworkManager.Singleton.Connect(usernameField.text, ipField.text);
    }

    // if for any reason we disconnect, go back to the UI.
    public void BackToMain()
    {
        usernameField.interactable = true;
        connectUI.SetActive(true);
    }

}
