using RiptideNetworking;
using RiptideNetworking.Utils;
using UnityEngine;

public enum ServerToClientId : ushort
{
    playerSpawned = 1,
    playerMovement = 2,
}

public enum ClientToServerId : ushort
{
    name = 1,
    input = 2,
}
public class NetworkManager : MonoBehaviour
{
    // https://gamedevbeginner.com/singletons-in-unity-the-right-way/

    // shared by all instances of the class, meaning that any script can access the singleton through its class name.
    private static NetworkManager _singleton;
    public static NetworkManager Singleton
    {
        get => _singleton;
        // can only be set from within its own class
        private set
        {
            if (_singleton == null)
                _singleton = value;
            else if (_singleton != value)
            {
                Debug.Log($"{nameof(NetworkManager)} instance already exists, destroying duplicate!");
                Destroy(value);
            }
        }
    }

    public Server Server { get; private set; }
        
    // make it accessible through the inspector, but not by any other scripts.
    [SerializeField] private ushort port;
    [SerializeField] private ushort maxClientCount;

    private void Awake()
    {
        Singleton = this;
    }

    private void Start()
    {
        Application.targetFrameRate = 60;

        // allow riptide logs to be logged in Unity console.
        RiptideLogger.Initialize(Debug.Log, Debug.Log, Debug.LogWarning, Debug.LogError, false);
           
        // start riptide server
        Server = new Server();
        Server.Start(port, maxClientCount);
        Server.ClientDisconnected += PlayerLeft;
    }

    private void FixedUpdate()
    {
        Server.Tick();
    }

    private void OnApplicationQuit()
    {
        Server.Stop();
    }

    private void PlayerLeft(object sender, ClientDisconnectedEventArgs e)
    {
        if (Player.list.TryGetValue(e.Id, out Player player))
            Destroy(player.gameObject);
    }
}