# Work2Gether



## Getting started

Steps to launch the game:
- Download latest release in the [Releases] folder
- Launch
- Use Ip: 34.78.51.33 to connect to the cloud server

You can issue issues on gitlab by taping i while on the project page! Don't hesitate to tell me about the thousands bugs incoming.

## Current features

### V0.1

First release of this beauty.
- [x] Basic server architecture.
- [x] cloud setup
- [x] Player movement 
- [x] Nametags
